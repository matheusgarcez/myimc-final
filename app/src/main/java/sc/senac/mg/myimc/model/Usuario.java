package sc.senac.mg.myimc.model;

import android.net.Uri;

public class Usuario {

    private Uri foto;
    private Float altura = 0F;
    private Float peso = 0F;
    private Float imc = 0F;
    private float satisfacao = 0F;

    public Float getAltura() {
        return altura;
    }

    public void setAltura(Float altura) {
        this.altura = altura;
    }

    public Float getPeso() {
        return peso;
    }

    public void setPeso(Float peso) {
        this.peso = peso;
    }

    public Uri getFoto() {
        return foto;
    }

    public void setFoto(Uri foto) {
        this.foto = foto;
    }

    public Float getImc() {
        return imc;
    }

    public void setImc(Float imc) {
        this.imc = imc;
    }

    public Situacao definirSituacao(Float imc) {
        if (imc < 18.5) {
            return Situacao.ABAIXO_PESO;
        } else if (imc < 24.9) {
            return Situacao.PESO_IDEAL;
        } else if (imc < 29.9) {
            return Situacao.SOBREPESO;
        } else if (imc < 34.9) {
            return Situacao.OBESIDADE_GRAU_I;
        } else if (imc < 39.9) {
            return Situacao.OBESIDADE_GRAU_II;
        } else {
            return Situacao.OBESIDADE_GRAU_III;
        }
    }

    public Float calcularValorIMC() {
        Float peso = this.getPeso();
        Float altura = this.getAltura();
        return peso / (altura * altura);
    }

    public void setSatisfacao(float satisfacao) {
        this.satisfacao = satisfacao;
    }

    public float getSatisfacao() {
        return satisfacao;
    }
}
