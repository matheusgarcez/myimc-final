package sc.senac.mg.myimc.model;

public enum Situacao {

    ABAIXO_PESO(1, "Abaixo do peso ideal"),
    PESO_IDEAL(2, "Peso ideal"),
    SOBREPESO(3, "Sobrepeso"),
    OBESIDADE_GRAU_I(4, "Obesidade grau I"),
    OBESIDADE_GRAU_II(5, "Obesidade grau II"),
    OBESIDADE_GRAU_III(6, "Obesidade grau III");

    private Integer situacao;
    private String descricao;

    Situacao(Integer situacao, String descricao) {
        this.situacao = situacao;
        this.descricao = descricao;
    }

    public Integer getSituacao() {
        return situacao;
    }

    public String getDescricao() {
        return descricao;
    }
}
