package sc.senac.mg.myimc;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import sc.senac.mg.myimc.model.Situacao;
import sc.senac.mg.myimc.model.Usuario;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String preferenceName = "MyIMC_Preferences";
    public static final String photoDirectory = "/imc.png";
    private static final String TAG = "MainActivity::class";

    private SharedPreferences preferences;
    private Usuario usuario;
    private ImageView fotoUsuario;
    private TextView situacaoPessoa;
    private EditText alturaPessoa;
    private EditText pesoPessoa;
    private RatingBar notaIMC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        preferences = getSharedPreferences(preferenceName, MODE_PRIVATE);

        notaIMC = (RatingBar) findViewById(R.id.notaIMC);
        pesoPessoa = (EditText) findViewById(R.id.peso);
        alturaPessoa = (EditText) findViewById(R.id.altura);
        fotoUsuario = (ImageView) findViewById(R.id.fotoUsuario);
        situacaoPessoa = (TextView) findViewById(R.id.situacao_pessoa);

        notaIMC.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                usuario.setSatisfacao(rating);
            }
        });

        //noinspection ConstantConditions
        findViewById(R.id.btn_calcular_imc).setOnClickListener(this);
        fotoUsuario.setOnClickListener(this);
        notaIMC.setOnClickListener(this);

        usuario = new Usuario();
        usuario.setAltura(preferences.getFloat("altura", 0));
        usuario.setPeso(preferences.getFloat("peso", 0));
        usuario.setImc(preferences.getFloat("imc", 0));
        usuario.setSatisfacao(preferences.getFloat("satisfacao", -1));

        String foto = preferences.getString("photoFile", "");

        if (!foto.isEmpty()) {
            usuario.setFoto(Uri.parse(foto));
            fotoUsuario.setImageURI(usuario.getFoto());
        } else {
            usuario.setFoto(null);
            fotoUsuario.setImageDrawable(getResources().getDrawable(R.drawable.semfoto));
        }

        if (usuario.getImc() > 0) {
            Situacao st = usuario.definirSituacao(usuario.getImc());
            situacaoPessoa.setText("IMC " + usuario.getImc() + " - " + st.getDescricao());
        }

        if (usuario.getSatisfacao() > -1) {
            notaIMC.setRating(usuario.getSatisfacao());
        }

        if (usuario.getPeso() > 0) {
            pesoPessoa.setText(usuario.getPeso().toString());
        }

        if (usuario.getAltura() > 0) {
            alturaPessoa.setText(usuario.getAltura().toString());
        }
    }

    @Override
    protected void onDestroy() {

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("photoFile", usuario.getFoto().toString());
        editor.putFloat("altura", usuario.getAltura());
        editor.putFloat("peso", usuario.getPeso());
        editor.putFloat("satisfacao", usuario.getSatisfacao());
        editor.putFloat("imc", usuario.calcularValorIMC());
        editor.putFloat("NotaIMC", usuario.getSatisfacao());
        editor.apply();

        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_calcular_imc:
                calcularIMC();
                break;
            case R.id.fotoUsuario:
                alterarFoto();
                break;
            case R.id.notaIMC:
                alterarNotaIMC();
                break;
        }
    }

    private void alterarNotaIMC() {
        Log.d("Alterado", String.valueOf(notaIMC.getRating()));
    }

    private static final int PERMISSION_CODE = 0x11;

    private void alterarFoto() {

        String[] permissions = {"android.permission.WRITE_EXTERNAL_STORAGE"};

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            Intent it = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(it, 0);
        } else {
            ActivityCompat.requestPermissions(this, permissions, PERMISSION_CODE);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent it = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(it, 0);
            } else {
                Toast.makeText(getApplicationContext(), "PERMISSION_DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 0 && resultCode == RESULT_OK) {

            Bundle extras = data.getExtras();
            Bitmap photo = (Bitmap) extras.get("data");

            fotoUsuario.setImageBitmap(photo);
            saveImage(photo);
        }
    }

    private void saveImage(Bitmap image) {

        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile = new File(path, timeStamp + ".jpg");

        try {
            path.mkdirs();
            mediaFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        OutputStream outputStream = null;

        try {
            outputStream = new FileOutputStream(mediaFile);
            image.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        usuario.setFoto(Uri.fromFile(mediaFile));
    }

    private void calcularIMC() {

        Float peso;
        Float altura;

        try {
            altura = Float.valueOf(alturaPessoa.getText().toString());
            peso = Float.valueOf(pesoPessoa.getText().toString());
        }catch (Exception ex) {
            Toast.makeText(MainActivity.this, "Preencha todos os campos corretamente.", Toast.LENGTH_SHORT).show();
            return;
        }

        usuario.setPeso(peso);
        usuario.setAltura(altura);

        Float imc = usuario.calcularValorIMC();
        Situacao situacao = usuario.definirSituacao(imc);

        usuario.setImc(imc);

        situacaoPessoa.setText("IMC " + usuario.getImc() + " - " + situacao.getDescricao());
    }

}
